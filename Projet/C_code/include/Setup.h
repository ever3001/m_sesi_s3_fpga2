#ifndef _SETUP_H_
#define _SETUP_H_

/************************************************/
/*********** Select number to analyze ***********/
/************************************************/
// #define INPUT_00                       // []
// #define INPUT_01                       // []
// #define INPUT_02                       // []
// #define INPUT_03                       // []
// #define INPUT_04                       // []
// #define INPUT_05                       // []
// #define INPUT_06                       // []
// #define INPUT_07                       // []
// #define INPUT_08                       // []
// #define INPUT_09                       // []
// #define INPUT_ERROR                    // []
#define USER_INPUT                     // [X]

/**********************************************/
/*********** Select an architecture ***********/
/**********************************************/
// #define PC                             // []
#define MCU                            // [X]


/**********************************************/
/******** Select a type of calc layer *********/
/**********************************************/
#ifdef MCU
#define HW_ACC
#endif

/**********************************************/
/*********** Select a type of float ***********/
/**********************************************/
// #define FLOAT32                        // []
// #define FLOAT16                        // [] // FIXME:
// #define FIXED16                        // [] // FIXME:
#define FIXED_INT16                       // [X]


/***************************************/
/*********** Select a Target ***********/
/***************************************/
// #define DEBUG                          // []
#define RELEASE                        // [X]

/**************************************************************/
/*********** Select infinite loop or iteration loop ***********/
/**************************************************************/
#define INFINITE_LOOP                  // [X]
// #define ITERATION_LOOP                 // []


/** DO NOT CHANGE THE NEXT CODE */

#ifdef ITERATION_LOOP
#define EXECUTION_TIMES (1)
#endif


#endif //! _SETUP_H_
