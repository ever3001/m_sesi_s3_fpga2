#ifndef _TIME_COUNT_H_
#define _TIME_COUNT_H_

#if defined(PC)
#include <time.h>

#define TIME_T clock_t
#define GET_CLOCK(clk) clk = clock()
#define GET_CLOCK_CYCLES(start, end) (end - start)
#define GET_TIME(start, end) (((double)(end - start)) / CLOCKS_PER_SEC)
#elif defined(MCU)
#include <xtime_l.h>

#define TIME_T XTime
#define GET_CLOCK(clk) XTime_GetTime(&clk)
#define GET_CLOCK_CYCLES(start, end) (2 * (end - start))
#define GET_TIME(start, end) (((double)(end - start)) / COUNTS_PER_SECOND)
#else
#endif

#endif //! _TIME_COUNT_H_