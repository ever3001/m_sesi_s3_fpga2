#include "calculateLayer1.h"
#include <string.h>

void calculateLayer1(const input_t* input, neuron_t* Layer1_Neurons_CPU){
  for (size_t i = 0; i < LAYER1_NEURON_SZ; ++i)
  {
    Layer1_Neurons_CPU[i] = float32_to_type((float)input[i]);
  }
}