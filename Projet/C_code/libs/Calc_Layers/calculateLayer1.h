#ifndef _CALCULATE_LAYER_1_H_
#define _CALCULATE_LAYER_1_H_
#include "calculateLayer_Setup.h"
#include "Input.h"

extern void calculateLayer1(const input_t* input, neuron_t* Layer1_Neurons_CPU);

#endif //! _CALCULATE_LAYER_1_H_