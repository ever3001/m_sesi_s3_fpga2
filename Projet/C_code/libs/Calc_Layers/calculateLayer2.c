#include "calculateLayer2.h"

#include <stdint.h>

void calculateLayer2(neuron_t* Layer1_Neurons_CPU, weight_t* Layer1_Weights_CPU, neuron_t* Layer2_Neurons_CPU){
  calc_t add_res;
  calc_t mul_res;
  uint8_t i,j,k,m,n;
  for(i=0;i<6;i++) 
    for(j=0;j<13;j++) 
      for(k=0;k<13;k++){
        add_res = Layer1_Weights_CPU[26*i]; 
        for(m=0;m<5;m++) 
          for(n=0;n<5;n++) 
          {
            mul_res = mul_two_points(Layer1_Weights_CPU[26*i+5*m+n+1], Layer1_Neurons_CPU[29*(m+2*j)+n+2*k]);
            add_res = add_two_points(add_res, mul_res);
          }
        Layer2_Neurons_CPU[13*13*i+13*j+k] = SIGMOID(add_res);
      }
}