#ifndef _CALCULATE_LAYER_2_H_
#define _CALCULATE_LAYER_2_H_
#include "calculateLayer_Setup.h"

extern void calculateLayer2(neuron_t* Layer1_Neurons_CPU, weight_t* Layer1_Weights_CPU, neuron_t* Layer2_Neurons_CPU);

#endif //! _CALCULATE_LAYER_2_H_