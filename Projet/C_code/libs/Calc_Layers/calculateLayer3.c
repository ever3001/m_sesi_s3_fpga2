#include "calculateLayer3.h"

#include <stdint.h>

void calculateLayer3(neuron_t* Layer2_Neurons_CPU, weight_t* Layer2_Weights_CPU, neuron_t* Layer3_Neurons_CPU){
  calc_t add_res;
  calc_t mul_res;
  uint8_t i,j,k,m,n;
  for( i=0;i<50;i++)
    for(j=0;j<5;j++)
      for(k=0;k<5;k++){
        add_res = Layer2_Weights_CPU[26*6*i];
        for( m=0;m<5;m++)
          for( n=0;n<5;n++){
            mul_res = mul_two_points(Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)	], Layer2_Neurons_CPU[13*13*0+13*(2*j+m)+(2*k+n)]);
            add_res = add_two_points(add_res, mul_res);
            mul_res = mul_two_points(Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+1], Layer2_Neurons_CPU[13*13*1+13*(2*j+m)+(2*k+n)]);
            add_res = add_two_points(add_res, mul_res);
            mul_res = mul_two_points(Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+2], Layer2_Neurons_CPU[13*13*2+13*(2*j+m)+(2*k+n)]);
            add_res = add_two_points(add_res, mul_res);
            mul_res = mul_two_points(Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+3], Layer2_Neurons_CPU[13*13*3+13*(2*j+m)+(2*k+n)]);
            add_res = add_two_points(add_res, mul_res);
            mul_res = mul_two_points(Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+4], Layer2_Neurons_CPU[13*13*4+13*(2*j+m)+(2*k+n)]);
            add_res = add_two_points(add_res, mul_res);
            mul_res = mul_two_points(Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+5], Layer2_Neurons_CPU[13*13*5+13*(2*j+m)+(2*k+n)]);
            add_res = add_two_points(add_res, mul_res);
          }
        Layer3_Neurons_CPU[5*5*i+5*j+k] = SIGMOID(add_res);
      }
}