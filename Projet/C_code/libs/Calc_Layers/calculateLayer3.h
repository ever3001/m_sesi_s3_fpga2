#ifndef _CALCULATE_LAYER_3_H_
#define _CALCULATE_LAYER_3_H_

#include "calculateLayer_Setup.h"

extern void calculateLayer3(neuron_t* Layer2_Neurons_CPU, weight_t* Layer2_Weights_CPU, neuron_t* Layer3_Neurons_CPU);


#endif //! _CALCULATE_LAYER_3_H_