#include "calculateLayer3_and_4_ip_mcu.h"

#include <stdbool.h>

#include <xcalculatelayer3_and_4.h>

#include "xparameters.h"
#include "platform.h"

#define LAYER2_NEURONS_BRAM_BASE_ADDRESS XPAR_AXI_BRAM_CTRL_0_S_AXI_BASEADDR
#define LAYER4_NEURONS_BRAM_BASE_ADDRESS XPAR_AXI_BRAM_CTRL_2_S_AXI_BASEADDR
#define RETURN_ERR_VAL (-1)

bool is_ip_init = false;
XCalculatelayer3_and_4 calcLayer3_and_4;
XCalculatelayer3_and_4_Config * calcLayer3_and_4_config;

neurons_hw_t *Layer2_Neurons_CPU_HW = (neurons_hw_t *) LAYER2_NEURONS_BRAM_BASE_ADDRESS;
neurons_hw_t *Layer4_Neurons_CPU_HW = (neurons_hw_t *) LAYER4_NEURONS_BRAM_BASE_ADDRESS;

void init_ip(){
    init_platform();
    calcLayer3_and_4_config = XCalculatelayer3_and_4_LookupConfig(XPAR_CALCULATELAYER3_AND_4_0_DEVICE_ID);
    if(! calcLayer3_and_4_config){
        xil_printf("[ERROR] Loading config for calcLayer3_and_4_config\r\n");
    }
    int status = XCalculatelayer3_and_4_CfgInitialize(&calcLayer3_and_4, calcLayer3_and_4_config);
    if(status != XST_SUCCESS){
        xil_printf("[ERROR] Initializing for calcLayer3_and_4_config\r\n");
    }
}


int calculateLayer3_and_4(
    neuron_t Layer2_Neurons_CPU[LAYER2_NEURON_SZ],
    neuron_t Layer4_Neurons_CPU[LAYER4_NEURON_SZ]
)
{
    int return_value = RETURN_ERR_VAL;
    if(!is_ip_init){
        init_ip();
        is_ip_init = true;
    }
    // Save the values of layer 2 into the BRAM
    for(int i = 0; i < LAYER2_NEURON_SZ ; i+= 1){
        Layer2_Neurons_CPU_HW[i] = Layer2_Neurons_CPU[i];
        // FOR DEBUG
        /*if (i < 20){
            xil_printf("Layer2_Neurons_CPU   [%d] =  %d\r\n", i, Layer2_Neurons_CPU[i]);
            xil_printf("Layer2_Neurons_CPU_HW[%d] =  %d\r\n", i, Layer2_Neurons_CPU_HW[i]);
        }*/
    }
    // Start the material IP
    XCalculatelayer3_and_4_Start(&calcLayer3_and_4);
    // Wait for the IP to finish
    while(!XCalculatelayer3_and_4_IsDone(&calcLayer3_and_4));
    // Get return value (normally is 0)
    return_value = XCalculatelayer3_and_4_Get_return(&calcLayer3_and_4);
    // Print the return value
    xil_printf("The return value is = %d\r\n", return_value);
    // Copy the values of the BRAM into the array to continue to next layer
    for(int i = 0; i < LAYER4_NEURON_SZ ; i+=1){
        Layer4_Neurons_CPU[i] = Layer4_Neurons_CPU_HW[i];
        // FOR DEBUG
        /*if (i < 20){
            printf("Layer3_Neurons_CPU   [%d] =  %d\r\n", i, Layer3_Neurons_CPU[i]);
            printf("Layer3_Neurons_CPU_HW[%d] =  %d %f\r\n", i, Layer3_Neurons_CPU_HW[i], TO_FLOAT(Layer3_Neurons_CPU_HW[i]));
        }*/
    }
    return return_value;
}