#ifndef _CALCULATE_LAYER_3_AND_4_IP_MCU_H_
#define _CALCULATE_LAYER_3_AND_4_IP_MCU_H_
#include <stdint.h>
#include "Neurons.h"

typedef int32_t neurons_hw_t;

extern int calculateLayer3_and_4(
    neuron_t Layer2_Neurons_CPU[LAYER2_NEURON_SZ],
    neuron_t Layer4_Neurons_CPU[LAYER4_NEURON_SZ]
);


#endif //! _CALCULATE_LAYER_3_AND_4_IP_MCU_H_