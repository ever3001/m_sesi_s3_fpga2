#include "calculateLayer4.h"

#include <stdint.h>

void calculateLayer4(neuron_t* Layer3_Neurons_CPU, weight_t* Layer3_Weights_CPU, neuron_t* Layer4_Neurons_CPU){
  calc_t add_res;
  calc_t mul_res;
  uint8_t i,j,k,m;
  for( i=0;i<100;i++){
    add_res = Layer3_Weights_CPU[i*(1+50*25)];
    for( j=0;j<50;j++) 
      for( k=0;k<5;k++) 
        for ( m=0;m<5;m++){
          mul_res = mul_two_points(Layer3_Weights_CPU[i*(1+50*25)+1 + m + k*5 + j*25],Layer3_Neurons_CPU[m+5*k+25*j]);
          add_res = add_two_points(add_res, mul_res);
        }
    Layer4_Neurons_CPU[i] = SIGMOID(add_res);
  }
}


