#ifndef _CALCULATE_LAYER_4_H_
#define _CALCULATE_LAYER_4_H_

#include "calculateLayer_Setup.h"

extern void calculateLayer4(neuron_t* Layer3_Neurons_CPU, weight_t* Layer3_Weights_CPU, neuron_t* Layer4_Neurons_CPU);


#endif //! _CALCULATE_LAYER_4_H_