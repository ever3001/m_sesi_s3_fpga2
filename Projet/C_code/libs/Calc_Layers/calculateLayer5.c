#include "calculateLayer5.h"

#include <stdint.h>

void calculateLayer5(neuron_t* Layer4_Neurons_CPU, weight_t* Layer4_Weights_CPU, neuron_t* Layer5_Neurons_CPU){
  calc_t add_res;
  calc_t mul_res;
  uint8_t i, j;
  for( i=0;i<10;i++){
    add_res = Layer4_Weights_CPU[101*i];
    for( j=0;j<100;j++){
      mul_res = mul_two_points(Layer4_Weights_CPU[1+101*i+j], Layer4_Neurons_CPU[j]);
      add_res = add_two_points(add_res, mul_res);
    }
    Layer5_Neurons_CPU[i] = SIGMOID(add_res);
  }
}