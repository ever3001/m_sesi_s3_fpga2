#ifndef _CALCULATE_LAYER_5_H_
#define _CALCULATE_LAYER_5_H_

#include "calculateLayer_Setup.h"

extern void calculateLayer5(neuron_t* Layer4_Neurons_CPU, weight_t* Layer4_Weights_CPU, neuron_t* Layer5_Neurons_CPU);

#endif //! _CALCULATE_LAYER_5_H_