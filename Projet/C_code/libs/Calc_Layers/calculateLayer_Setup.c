#include "calculateLayer_Setup.h"

#if defined(FLOAT32)

calc_t mul_two_points(weight_t w, neuron_t n) { return w * n; }

calc_t add_two_points(calc_t a, calc_t b) { return a + b; }

float type_to_float32(base_type_t a) { return a; }

base_type_t float32_to_type(float a) { return a; }

#elif defined(FLOAT16)

calc_t mul_two_points(weight_t w, neuron_t n) { return f16_mul(w, n); }

calc_t add_two_points(calc_t a, calc_t b) { return f16_add(a, b); }

float type_to_float32(base_type_t a) { return float16_to_float32(a); }

base_type_t float32_to_type(float a) { return float32_to_float16(a); }

#elif defined(FIXED16)

calc_t mul_two_points(weight_t w, neuron_t n) { return mul_fix16(w, n); }

calc_t add_two_points(calc_t a, calc_t b) { return add_fix16(a, b); }

float type_to_float32(base_type_t a) { return fixed16_to_float32(a); }

base_type_t float32_to_type(float a) { return float32_to_fixed16(a); }

#elif defined(FIXED_INT16)

calc_t mul_two_points(weight_t w, neuron_t n) { return FIX_MUL(w, n); }

calc_t add_two_points(calc_t a, calc_t b) { return a+b; }

float type_to_float32(base_type_t a) { return TO_FLOAT(a); }

base_type_t float32_to_type(float a) { return TO_FIX(a); }

#endif
