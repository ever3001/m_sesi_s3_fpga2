#ifndef _CALCULATE_LAYER_SETUP_H_
#define _CALCULATE_LAYER_SETUP_H_

#include <math.h>

#include "Neurons.h"
#include "Weights.h"

#if defined(FLOAT32)

typedef float calc_t;
typedef float base_type_t;

#elif defined(FLOAT16)
#include "float16.h"
#include "conv.h"

typedef float16_t calc_t;
typedef float16_t base_type_t;

#elif defined(FIXED16)
#include "fixed16.h"

typedef fixed16_t calc_t;
typedef fixed16_t base_type_t;

#elif defined(FIXED_INT16)
#include "fixed_int16.h"

typedef int32_t calc_t;
typedef int16_t base_type_t;

#endif




extern calc_t mul_two_points(weight_t w, neuron_t n);
extern calc_t add_two_points(calc_t a, calc_t b);
extern float type_to_float32(base_type_t a);
extern base_type_t float32_to_type(float a);

/** MACROS */

#if defined(FIXED16)

//TODO:

#elif defined(FLOAT16)
// TODO:

#elif defined(FIXED_INT16)
#define SIGMOID(x) sigmoid_approx(x)

#else

#define SIGMOID(x) (1.7159*tanh(0.66666667*x))
#define DSIGMOID(S) (0.66666667/1.7159*(1.7159+(S))*(1.7159-(S)))

#endif


#endif //! _CALCULATE_LAYER_SETUP_H_