#include "fixed16.h"

#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#define POSITIVE_BIT_VAL (0)
#define NEGATIVE_BIT_VAL (1)

#define OVERFLOW_VAL (0xFFFF)

#define N ((1 << FRAC_BIT_SZ)) // 4096 for 12 bits
#define MAX_VALUE_BITS(b) ((1 << b) - 1)

fixed16_t float32_to_fixed16(float f) {
  fixed16_t fixed;
  if ((uint8_t)abs(f) > MAX_VALUE_BITS(INT_BIT_SZ)) { // ! OVERFLOW
    // Change value to overflow
    fixed.val = OVERFLOW_VAL;
  } else {
    fixed.val = 0;
    // Is float value negative ( < 0 )?
    fixed.sign = (f < 0) ? NEGATIVE_BIT_VAL : POSITIVE_BIT_VAL;
    // Get the absolute integer value of the float
    fixed.int_part = (uint8_t)abs(f);
    // Get the absolute fractional value of the float
    fixed.frac_part = (uint16_t)(fabsf((f - (int8_t)f) * N));
  }
  return fixed;
}

float fixed16_to_float32(fixed16_t f) {
  // Initiate the float value with the integer part of the fixed point
  float f32 = (float)f.int_part;
  // Divide the fractional part with the constant N
  f32 += (float)f.frac_part / (float)N;
  // Multiply by -1 if f.sign = 1
  f32 *= (f.sign) ? -1 : 1;
  return f32;
}

void print_fixed16_t(fixed16_t f) {
  float buff = fixed16_to_float32(f);
  if (f.val == 0xFFFF) {
    printf("[ERROR] OVERFLOW\r\n");
  } else {
    printf("Sign             = %s\r\n", (f.sign ? "-" : "+"));
    printf("Integer part     = %d\r\n", f.int_part);
    printf("Fractional part  = %d\r\n", f.frac_part);
    printf("Hex value        = 0x%04x\r\n", f.val);
    printf("Fixed 16 value   = %f\r\n", buff);
  }
}

fixed16_t add_fix16(fixed16_t a, fixed16_t b) {
  float a_f = fixed16_to_float32(a);
  float b_f = fixed16_to_float32(b);
  return float32_to_fixed16(a_f + b_f);
}
// fixed16_t add_fix16(fixed16_t a, fixed16_t b){
//     fixed16_t c = {0};
//     int32_t c_int = 0;
//     int32_t c_frac = 0;
//     uint8_t c_sign = 0;
//     uint8_t overflow_flag = 0;

//     if(!(a.sign ^ b.sign)){ // Addition
//         c_int = a.int_part + b.int_part
//         c_frac = a.int_part - b.int_part

//     }else{                  // Substraction

//     }

//     int8_t a_sign = (a.sign) ? -1 : 1;
//     int8_t b_sign = (b.sign) ? -1 : 1;

//     fix32.int_part = a_sign*a.int_part + b_sign*b.int_part;
//     fix32.frac_part = a_sign*a.frac_part + b_sign*b.frac_part;

//     fix32.sign = (a.val & ((1 << 15+1)-1) > b.val & ((1 << 15+1)-1)) ? a.sign
//     : b.sign;

//     fix32.int_part += fix32.frac_part >> FRAC_BIT_SZ;
//     overflow_flag = (fix32.int_part >> INT_BIT_SZ > 0) ? 1 : 0;

//     c.int_part  = (fix32.int_part & ((1 << INT_BIT_SZ+1)-1));
//     c.frac_part = (fix32.frac_part & ((1 << FRAC_BIT_SZ+1)-1));
//     c.sign      = fix32.sign & ((1 << 1+1)-1);

//     return c;
// }

fixed16_t mul_fix16(fixed16_t a, fixed16_t b) {
  float a_f = fixed16_to_float32(a);
  float b_f = fixed16_to_float32(b);
  return float32_to_fixed16(a_f * b_f);
}