#ifndef _FIXED16_H_
#define _FIXED16_H_

#include <stdint.h>


/*################################ SETUP */
/* Change the value of the fractional and integer part. The sum of the two parts must be <= 15  */
#define FRAC_BIT_SZ (10)
#define INT_BIT_SZ  (5)

#define SIGN_BIT_SZ (1) // ! Don't change

typedef union
{
    uint16_t        val;
    struct
    {
        uint16_t    frac_part   :FRAC_BIT_SZ;   // Steps = 2^(-FRAC_BIT_SZ)
        uint16_t    int_part    :INT_BIT_SZ;    // -INT_BIT_SZ to INT_BIT_SZ
        uint16_t    sign        :SIGN_BIT_SZ;   // Negative = 1; Positive = 0;
    };
}fixed16_t;


extern fixed16_t    float32_to_fixed16  (float f);
extern float        fixed16_to_float32  (fixed16_t f);
extern void         print_fixed16_t     (fixed16_t f);
extern fixed16_t    add_fix16           (fixed16_t a, fixed16_t b);
extern fixed16_t    mul_fix16           (fixed16_t a, fixed16_t b);

#endif //? _FIXED16_H_