#include "fixed_int16.h"

int16_t sigmoid_approx(int16_t x) {

  int16_t x3, x5;
  // 1.14393*x - 0.169472*x^3 + 0.0301283*x^5
  if (TO_FLOAT(x) > 1.5)
    return TO_FIX(1.7);
  if (TO_FLOAT(x) < -1.5)
    return TO_FIX(-1.7);
  x3 = x + x + x;
  x5 = x3 + x + x;
  return FIX_MUL(TO_FIX(1.14393), x) - FIX_MUL(TO_FIX(0.169472), x3) +
         FIX_MUL(TO_FIX(0.0301283), x5);

  // return TO_FIX(SIGMOID(TO_FLOAT(x)));
}
