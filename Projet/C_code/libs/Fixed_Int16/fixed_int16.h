#ifndef _FIXED_INT_16_H_
#define _FIXED_INT_16_H_
#include <stdint.h>

#define NBIT 8

#define TO_FIX(x)    ((int16_t)(x*(1<<NBIT)))
#define TO_FLOAT(x)  ((float)x / (float)(1<<NBIT))
#define FIX_MUL(x,y) ((x*y)/(1<<NBIT))
#define FIX_DIV(x,y) ((x/y)/(1<<NBIT))

extern int16_t sigmoid_approx(int16_t x);

#endif //! _FIXED_INT_16_H_