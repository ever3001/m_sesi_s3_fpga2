#ifndef F1632_CONV_H
#define F1632_CONV_H

typedef struct Float32 {
    union {
        unsigned value;
        float fvalue;
        struct {
            unsigned fraction: 23;
            unsigned exponent: 8;
            unsigned sign: 1;
        };
    } m;
} Float32;
typedef unsigned short float16_t;

float16_t float32_to_float16(float v);
float float16_to_float32(float16_t v);

#endif
