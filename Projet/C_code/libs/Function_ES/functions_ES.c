#include "functions_ES.h"

#include <stdio.h>
#include <stdlib.h>

void InitHostMem(
  weight_t Layer1_Weights_CPU[LAYER1_WEIGHTS_SZ],
  weight_t Layer2_Weights_CPU[LAYER2_WEIGHTS_SZ],
  weight_t Layer3_Weights_CPU[LAYER3_WEIGHTS_SZ],
  weight_t Layer4_Weights_CPU[LAYER4_WEIGHTS_SZ]) 
{
  // initial layer 1 weight
  // Open the file for reading. b = binary file
  FILE *pFile1 = fopen(FILE_LAYER1, "rb");
  // If error opening the file
  if (pFile1 != NULL) {
    fread(Layer1_Weights_CPU, sizeof(weight_t), LAYER1_WEIGHTS_SZ, pFile1);
    fclose(pFile1);
  }else{ 
    perror("[Error][FILE_LAYER1]");
    exit(1);
  }

  // initial layer 2 weight
  FILE *pFile2 = fopen(FILE_LAYER2, "rb");
  if (pFile2 != NULL) {
    fread(Layer2_Weights_CPU, sizeof(weight_t), LAYER2_WEIGHTS_SZ, pFile2);
    fclose(pFile2);
  }else{ 
    perror("[Error][FILE_LAYER2]");
    exit(2);
  }
  // initial layer 3 weight
  FILE *pFile3 = fopen(FILE_LAYER3, "rb");
  if (pFile3 != NULL) {
    fread(Layer3_Weights_CPU, sizeof(weight_t), LAYER3_WEIGHTS_SZ, pFile3);
    fclose(pFile3);
  }else{ 
    perror("[Error][FILE_LAYER3]");
    exit(3);
  }
  // initial layer 4 weight
  FILE *pFile4 = fopen(FILE_LAYER4, "rb");
  if (pFile4 != NULL) {
    fread(Layer4_Weights_CPU, sizeof(weight_t), LAYER4_WEIGHTS_SZ, pFile4);
    fclose(pFile4);
  }else{ 
    perror("[Error][FILE_LAYER4]");
    exit(4);
  }
}

// void readIn(weight_t *layer1) {
//   FILE *fp;
//   fp = fopen("in.neu", "rb");
//   if (fp) {
//     fread(layer1, sizeof(weight_t), 29 * 29, fp);
//     fclose(fp);
//   }
// }

// void output(weight_t *final) {
//   FILE *fp = 0;
//   fp = fopen("out.res", "wb");
//   if (fp) {
//     fwrite(final, sizeof(weight_t), 10, fp);
//     fclose(fp);
//   }
// }
