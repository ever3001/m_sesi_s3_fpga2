#ifndef _FUNCTION_ES_H_
#define _FUNCTION_ES_H_

#include "Weights.h"

extern void InitHostMem(weight_t Layer1_Weights_CPU[LAYER1_WEIGHTS_SZ],
                        weight_t Layer2_Weights_CPU[LAYER2_WEIGHTS_SZ],
                        weight_t Layer3_Weights_CPU[LAYER3_WEIGHTS_SZ],
                        weight_t Layer4_Weights_CPU[LAYER4_WEIGHTS_SZ]);
// extern void readIn(weight_t *layer1);
// extern void output(weight_t *final);

#endif //! _FUNCTION_ES_H_