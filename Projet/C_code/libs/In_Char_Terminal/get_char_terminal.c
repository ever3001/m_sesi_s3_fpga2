#include "get_char_terminal.h"
#include "Neurons.h"

#include <stdio.h>
#include <stdint.h>
#include <string.h>

void get_input_in_terminal(input_t *input, size_t sz) {
  char in_val[sz + IMGWIDTH * 2];
  char c;
  size_t i = 0;
  strcpy(in_val, "");
  printf("Please enter the character file: \r\n");
  while (1) {
    c = getchar();
    strncat(in_val, &c, 1);
    if (c == '1') {
      input[i++] = (input_t)1;
    } else if (c == '0') {
      input[i++] = (input_t)0;
    }
    if (i >= sz) {
      break;
    }
  }
  printf("\r\n");
  printf("You entered: \r\n");
  printf("%s\r\n", in_val);
}