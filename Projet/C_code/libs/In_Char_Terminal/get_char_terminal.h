#ifndef _GET_CHAR_TERMINAL_H_
#define _GET_CHAR_TERMINAL_H_

#include "Input.h"
#include <stddef.h>

extern void get_input_in_terminal(input_t *input, size_t sz);

#endif // !_GET_CHAR_TERMINAL_H_