#include "get_char_terminal.h"

#include "xuartps.h"

#define UART_BASEADDR XPAR_PS7_UART_1_BASEADDR

void get_input_in_terminal(input_t *input, size_t sz) {
  char in_val[sz + IMGWIDTH * 2];
  char c;
  u32 CntrlRegister;
  size_t i = 0;
  strcpy(in_val, "");
  CntrlRegister = XUartPs_ReadReg(UART_BASEADDR, XUARTPS_CR_OFFSET);
  XUartPs_WriteReg(UART_BASEADDR, XUARTPS_CR_OFFSET,
                   ((CntrlRegister & ~XUARTPS_CR_EN_DIS_MASK) |
                    XUARTPS_CR_TX_EN | XUARTPS_CR_RX_EN));
  xil_printf("Please enter the character file: \r\n");
  while (1) {
    // Wait for input from UART via the terminal
    while (!XUartPs_IsReceiveData(UART_BASEADDR))
      ;
    c = XUartPs_ReadReg(UART_BASEADDR, XUARTPS_FIFO_OFFSET);
    strncat(in_val, &c, 1);
    if (c == '1') {
      input[i++] = (input_t)1;
    } else if (c == '0') {
      input[i++] = (input_t)0;
    }
    if (i >= sz) {
      break;
    }
  }
  xil_printf("\r\n");
}