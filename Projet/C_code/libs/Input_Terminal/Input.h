#ifndef _INPUT_H_
#define _INPUT_H_
#include <stdint.h>

#include "Input_Setup.h"
#include "Neurons.h"

#define INPUT_SIZE LAYER1_NEURON_SZ

extern input_t _CHAR_INPUT[INPUT_SIZE];

extern void force_input_to_0(void);
extern void force_input_to_1(void);
extern void force_input_to_2(void);
extern void force_input_to_3(void);
extern void force_input_to_4(void);
extern void force_input_to_5(void);
extern void force_input_to_6(void);
extern void force_input_to_7(void);
extern void force_input_to_8(void);
extern void force_input_to_9(void);
extern void force_input_to_error(void);
extern void clear_input(void);

#endif //! _INPUT_H_