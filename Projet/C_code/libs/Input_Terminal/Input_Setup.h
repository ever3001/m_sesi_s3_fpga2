#ifndef _INPUT_SETUP_H_
#define _INPUT_SETUP_H_

#include "Setup.h"
#include <stdint.h>

#if defined(FLOAT32)
typedef uint16_t  input_t;
#elif defined(FLOAT16)
#include "float16.h"
#include "conv.h"
typedef uint16_t  input_t;
#elif defined(FIXED16)
#include "fixed16.h"
typedef uint16_t  input_t;
#elif defined(FIXED_INT16)
#include "fixed_int16.h"
typedef int16_t  input_t;
#endif

#endif //! _INPUT_SETUP_H_