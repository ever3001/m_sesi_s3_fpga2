#ifndef _LEDS_H_
#define _LEDS_H_

#include "Led_Setup.h"

#include <stdint.h>

extern int init_gpio_leds     (void);
extern int display_number_leds(uint8_t val_7_bit);

#endif //! _LEDS_H_