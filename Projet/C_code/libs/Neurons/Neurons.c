#include "Neurons.h"

neuron_t         Layer1_Neurons_CPU[LAYER1_NEURON_SZ] = {0};
neuron_t         Layer2_Neurons_CPU[LAYER2_NEURON_SZ] = {0};
neuron_t         Layer3_Neurons_CPU[LAYER3_NEURON_SZ] = {0};
neuron_t         Layer4_Neurons_CPU[LAYER4_NEURON_SZ] = {0};
neuron_t         Layer5_Neurons_CPU[LAYER5_NEURON_SZ] = {0};