#ifndef _NEURONS_H_
#define _NEURONS_H_

#include"Neurons_Setup.h"

#define IMGWIDTH (29)
#define IMGHEIGHT (29)
#define LAYER1_NEURON_SZ (IMGWIDTH * IMGHEIGHT) // 841
#define LAYER2_NEURON_SZ (1014)                 // 6*13*13
#define LAYER3_NEURON_SZ (1250)                 // 50*5*5
#define LAYER4_NEURON_SZ (100)                  // 10*10
#define LAYER5_NEURON_SZ (10)

/* Layers Neurons*/
extern neuron_t         Layer1_Neurons_CPU[LAYER1_NEURON_SZ];
extern neuron_t         Layer2_Neurons_CPU[LAYER2_NEURON_SZ];
extern neuron_t         Layer3_Neurons_CPU[LAYER3_NEURON_SZ];
extern neuron_t         Layer4_Neurons_CPU[LAYER4_NEURON_SZ];
extern neuron_t         Layer5_Neurons_CPU[LAYER5_NEURON_SZ];

#endif //! _NEURONS_H_