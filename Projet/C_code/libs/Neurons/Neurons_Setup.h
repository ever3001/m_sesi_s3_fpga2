#ifndef _NEURONS_SETUP_H_
#define _NEURONS_SETUP_H_

#include "Setup.h"

#if defined(FLOAT32)
typedef float   neuron_t;
#elif defined(FLOAT16)
#include "float16.h"
#include "conv.h"

typedef float16_t   neuron_t;
#elif defined(FIXED16)
#include "fixed16.h"

typedef fixed16_t   neuron_t;

#elif defined(FIXED_INT16)
#include "fixed_int16.h"

typedef int16_t   neuron_t;

#endif

#endif //! _NEURONS_SETUP_H_