#ifndef _WEIGHTS_H_
#define _WEIGHTS_H_

#include "Weights_Setup.h"

#define LAYER1_WEIGHTS_SZ (156)    // (5 * 5 + 1) * 6
#define LAYER2_WEIGHTS_SZ (7800)   // (5 * 5 + 1) * 6 * 50
#define LAYER3_WEIGHTS_SZ (125100) // (5 * 5 * 50 + 1) * 100
#define LAYER4_WEIGHTS_SZ (1010)   // (100 + 1) * 10

#define FILE_LAYER1 (PATH_WEIGHTS "lw1.wei")
#define FILE_LAYER2 (PATH_WEIGHTS "lw2.wei")
#define FILE_LAYER3 (PATH_WEIGHTS "lw3.wei")
#define FILE_LAYER4 (PATH_WEIGHTS "lw4.wei")

extern weight_t Layer1_Weights_CPU[LAYER1_WEIGHTS_SZ];
extern weight_t Layer2_Weights_CPU[LAYER2_WEIGHTS_SZ];
extern weight_t Layer3_Weights_CPU[LAYER3_WEIGHTS_SZ];
extern weight_t Layer4_Weights_CPU[LAYER4_WEIGHTS_SZ];

#endif //! _WEIGHTS_H_