#ifndef _WEIGHTS_SETUP_H_
#define _WEIGHTS_SETUP_H_

#include "Setup.h"

#if defined(FLOAT32)
#define PATH_WEIGHTS "../../data/weights/float32/"
typedef float weight_t;
#elif defined(FLOAT16)
#include "float16.h"
#include "conv.h"
#define PATH_WEIGHTS "../../data/weights/float16/"
typedef float16_t weight_t;
#elif defined(FIXED16)
#include "fixed16.h"
#define PATH_WEIGHTS "../../data/weights/fixed/fixed_16/"
typedef fixed16_t weight_t;
#elif defined(FIXED_INT16)
#include "fixed_int16.h"
#define PATH_WEIGHTS "../../data/weights/fixed/fixed_int16/"
typedef int16_t weight_t;
#endif

#endif //! _WEIGHTS_SETUP_H_