#include <float.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>

#include "Input.h"
#include "Neurons.h"
#include "Weights.h"
#include "calc_layers.h"
#include "functions_ES.h"
#include "print_c.h"
#include "time_c.h"
#include "get_char_terminal.h"

#ifdef MCU
#include "Led.h"
#include <ZedboardOLED.h>

char oled_text[16];
#ifdef HW_ACC
#include <xcalculatelayer3_and_4.h>
#endif // ! HW_ACC
#include "calculateLayer3_and_4_ip_mcu.h"

#endif // ! MCU

#define ERROR_VAL (-1)
#define FACTOR_TO_MUL (1000000) 

int main(int argc, char const *argv[]) {
  // Variables for counting clock time
  TIME_T start_clock, end_clock;
  // Variables for counting clock
  uint32_t InitHostMem_ck;
  uint32_t CalculateLayer1_ck;
  uint32_t CalculateLayer2_ck;
  uint32_t CalculateLayer3_ck;
  uint32_t CalculateLayer4_ck;
  uint32_t CalculateLayer5_ck;
  // Variables for counting calculate the time
  float InitHostMem_us;
  float CalculateLayer1_us;
  float CalculateLayer2_us;
  float CalculateLayer3_us;
  float CalculateLayer4_us;
  float CalculateLayer5_us;
  // Variables to find the final result
  float score_max;
  int index_max;
#if defined(INPUT_00)
  force_input_to_0();
#elif defined(INPUT_01)
  force_input_to_1();
#elif defined(INPUT_02)
  force_input_to_2();
#elif defined(INPUT_03)
  force_input_to_3();
#elif defined(INPUT_04)
  force_input_to_4();
#elif defined(INPUT_05)
  force_input_to_5();
#elif defined(INPUT_06)
  force_input_to_6();
#elif defined(INPUT_07)
  force_input_to_7();
#elif defined(INPUT_08)
  force_input_to_8();
#elif defined(INPUT_09)
  force_input_to_9();
#elif defined(INPUT_ERROR)
  force_input_to_error();
#else
  clear_input();
#endif

  PRINT_F("Initiating program...\r\n");
  PRINT_F("\r\n");
#ifdef MCU
  clear();
  print_message("Please enter",0);
  print_message(" the input",1);
  print_message("  (0-9)",2);
#endif

#if defined(ITERATION_LOOP)
  for (size_t i = 0; i < EXECUTION_TIMES; ++i) {
    PRINT_F("####################### Iteration = %u\r\n\r\n", i + 1);
#else
  while (1) {
#endif // !ITERATION_LOOP
#ifdef USER_INPUT
    get_input_in_terminal(_CHAR_INPUT, LAYER1_NEURON_SZ);
#endif
    // Initialise the clock and time
    InitHostMem_ck     = 0; InitHostMem_us = 0.f;
    CalculateLayer1_ck = 0; CalculateLayer1_us = 0.f;
    CalculateLayer2_ck = 0; CalculateLayer2_us = 0.f;
    CalculateLayer3_ck = 0; CalculateLayer3_us = 0.f;
    CalculateLayer4_ck = 0; CalculateLayer4_us = 0.f;
    CalculateLayer5_ck = 0; CalculateLayer5_us = 0.f;
    // Init Threshold to find the number match
    score_max = FLT_MIN;
    // Init final index with error val for the condition at the end
    index_max = ERROR_VAL;
    /*######################################################## InitHostMem 
    */
    DEBUG_PRINT("InitHostMem\r\n");
    GET_CLOCK(start_clock);                                                   // 1. Init chronometer
    InitHostMem(Layer1_Weights_CPU, Layer2_Weights_CPU, 
                Layer3_Weights_CPU, Layer4_Weights_CPU);                      // 2. Do the function
    GET_CLOCK(end_clock);                                                     // 3. Stop the chronometer
    InitHostMem_ck = GET_CLOCK_CYCLES(start_clock, end_clock);                // 4. Save cycles
    InitHostMem_us = GET_TIME(start_clock, end_clock) * FACTOR_TO_MUL;        // 5. Calculate time (in usec)

    /*######################################################## CalculateLayer1
     */
    /* Calculate each layer using the input value (0-9) */
    DEBUG_PRINT("CalculateLayer1\r\n");
    GET_CLOCK(start_clock);                                                   // 1. Init chronometer
    calculateLayer1(_CHAR_INPUT, Layer1_Neurons_CPU);                         // 2. Do the function
    GET_CLOCK(end_clock);                                                     // 3. Stop the chronometer
    CalculateLayer1_ck = GET_CLOCK_CYCLES(start_clock, end_clock);            // 4. Save cycles
    CalculateLayer1_us = GET_TIME(start_clock, end_clock) * FACTOR_TO_MUL;    // 5. Calculate time (in usec)

    /*######################################################## CalculateLayer2
     */
    DEBUG_PRINT("CalculateLayer2\r\n");
    GET_CLOCK(start_clock);                                                   // 1. Init chronometer
    calculateLayer2(Layer1_Neurons_CPU, Layer1_Weights_CPU, 
                    Layer2_Neurons_CPU);                                      // 2. Do the function
    GET_CLOCK(end_clock);                                                     // 3. Stop the chronometer
    CalculateLayer2_ck = GET_CLOCK_CYCLES(start_clock, end_clock);            // 4. Save cycles
    CalculateLayer2_us = GET_TIME(start_clock, end_clock) * FACTOR_TO_MUL;    // 5. Calculate time (in usec)
#ifdef HW_ACC
    /*######################################################## CalculateLayer3_and_4
     */
    DEBUG_PRINT("CalculateLayer3_and_4\r\n");
    GET_CLOCK(start_clock);                                                   // 1. Init chronometer
    calculateLayer3_and_4(Layer2_Neurons_CPU, Layer4_Neurons_CPU);            // 2. Do the function
    GET_CLOCK(end_clock);                                                     // 3. Stop the chronometer
    CalculateLayer3_ck = GET_CLOCK_CYCLES(start_clock, end_clock);            // 4. Save cycles
    CalculateLayer3_us = GET_TIME(start_clock, end_clock) * FACTOR_TO_MUL;    // 5. Calculate time (in usec)
#else
    /*######################################################## CalculateLayer3
     */
    DEBUG_PRINT("CalculateLayer3\r\n");
    GET_CLOCK(start_clock);                                                   // 1. Init chronometer
    calculateLayer3(Layer2_Neurons_CPU, Layer2_Weights_CPU, 
                    Layer3_Neurons_CPU);                                      // 2. Do the function
    GET_CLOCK(end_clock);                                                     // 3. Stop the chronometer
    CalculateLayer3_ck = GET_CLOCK_CYCLES(start_clock, end_clock);            // 4. Save cycles
    CalculateLayer3_us = GET_TIME(start_clock, end_clock) * FACTOR_TO_MUL;    // 5. Calculate time (in usec)

    /*######################################################## CalculateLayer4
     */
    DEBUG_PRINT("CalculateLayer4\r\n");
    GET_CLOCK(start_clock);                                                   // 1. Init chronometer
    calculateLayer4(Layer3_Neurons_CPU, Layer3_Weights_CPU, 
                    Layer4_Neurons_CPU);                                      // 2. Do the function
    GET_CLOCK(end_clock);                                                     // 3. Stop the chronometer
    CalculateLayer4_ck = GET_CLOCK_CYCLES(start_clock, end_clock);            // 4. Save cycles
    CalculateLayer4_us = GET_TIME(start_clock, end_clock) * FACTOR_TO_MUL;    // 5. Calculate time (in usec)
#endif
    /*######################################################## CalculateLayer5
     */
    DEBUG_PRINT("CalculateLayer5\r\n");
    GET_CLOCK(start_clock);                                                   // 1. Init chronometer
    calculateLayer5(Layer4_Neurons_CPU, Layer4_Weights_CPU, 
                    Layer5_Neurons_CPU);                                      // 2. Do the function
    GET_CLOCK(end_clock);                                                     // 3. Stop the chronometer
    CalculateLayer5_ck = GET_CLOCK_CYCLES(start_clock, end_clock);            // 4. Save cycles
    CalculateLayer5_us = GET_TIME(start_clock, end_clock) * FACTOR_TO_MUL;    // 5. Calculate time (in usec)
    DEBUG_PRINT("\r\n");

    /*######################################################## Finding number
     */
    for (uint8_t i = 0; i < LAYER5_NEURON_SZ; ++i) {
      // Get value of the last layer
      float l5_n_buff = type_to_float32(Layer5_Neurons_CPU[i]);
      // Convert it into int for MCU
      uint8_t int_part = (uint8_t) fabsf(l5_n_buff);
      uint32_t float_part = (uint32_t)((fabsf(l5_n_buff) - abs(int_part)) * FACTOR_TO_MUL);
      // Print the value of the each neuron of the layer 5
      DEBUG_PRINT("%d : %s%d.%lu\r\n", (int)i, l5_n_buff < 0.f ? "-" : "", int_part, float_part);
      // If value is greater than the score max, then we found the number
      if (l5_n_buff > score_max) {
        // Save the value of the neuron
        score_max = l5_n_buff;
        // Save the index as number
        index_max = i;
      }
    }
    /*######################################################## Handle Errors
     */
#ifdef MCU
    clear(); // Clear Oled
    if (index_max < 0){ // Display error value in leds (binary) and Oled
      display_number_leds(UINT8_MAX);
      print_message("ERROR X.X",0);
    }else{              // Display value in leds (binary) and Oled
      display_number_leds(index_max);
      print_message("The value is:",0);
      sprintf(oled_text, "%d", index_max);
      print_message(oled_text,2);
    }
#endif
    if (index_max < 0) {
      PRINT_F("[Error] Finding number \r\n");
    }
    /*######################################################## Print Results
     */
    else{
      // Print the result in the terminal
      PRINT_F("The value found is :\t%02d \r\n", index_max);
      PRINT_F("\r\n");
      DEBUG_PRINT("    InitHostMem Clock = %7lu cyles\r\n", InitHostMem_ck);
      DEBUG_PRINT("CalculateLayer1 Clock = %7lu cyles\r\n", CalculateLayer1_ck);
      DEBUG_PRINT("CalculateLayer2 Clock = %7lu cyles\r\n", CalculateLayer2_ck);
      DEBUG_PRINT("CalculateLayer3 Clock = %7lu cyles\r\n", CalculateLayer3_ck);
      DEBUG_PRINT("CalculateLayer4 Clock = %7lu cyles\r\n", CalculateLayer4_ck);
      DEBUG_PRINT("CalculateLayer5 Clock = %7lu cyles\r\n", CalculateLayer5_ck);
      DEBUG_PRINT("\r\n");
      DEBUG_PRINT("    InitHostMem Time  = %7lu uSec\r\n", (uint32_t) InitHostMem_us);
      DEBUG_PRINT("CalculateLayer1 Time  = %7lu uSec\r\n", (uint32_t) CalculateLayer1_us);
      DEBUG_PRINT("CalculateLayer2 Time  = %7lu uSec\r\n", (uint32_t) CalculateLayer2_us);
      DEBUG_PRINT("CalculateLayer3 Time  = %7lu uSec\r\n", (uint32_t) CalculateLayer3_us);
      DEBUG_PRINT("CalculateLayer4 Time  = %7lu uSec\r\n", (uint32_t) CalculateLayer4_us);
      DEBUG_PRINT("CalculateLayer5 Time  = %7lu uSec\r\n", (uint32_t) CalculateLayer5_us);
      PRINT_F("\r\n");
    }
  }
  return 0;
}