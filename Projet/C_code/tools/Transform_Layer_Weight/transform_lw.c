#include <stdio.h>

#include "../../libs/Fixed16/fixed16.h"

#define FILE_NAME_WEIGHTS_1 ("../../data/weights/float32/lw1.wei")
#define FILE_NAME_WEIGHTS_2 ("../../data/weights/float32/lw2.wei")
#define FILE_NAME_WEIGHTS_3 ("../../data/weights/float32/lw3.wei")
#define FILE_NAME_WEIGHTS_4 ("../../data/weights/float32/lw4.wei")

#define F16_FILE_NAME_WEIGHTS_1 ("transformed_lw1.wei")
#define F16_FILE_NAME_WEIGHTS_2 ("transformed_lw2.wei")
#define F16_FILE_NAME_WEIGHTS_3 ("transformed_lw3.wei")
#define F16_FILE_NAME_WEIGHTS_4 ("transformed_lw4.wei")

/** CONSTANTS */
#define LAYER1_WEIGHTS_SZ (156)    // (5 * 5 + 1) * 6         = 156
#define LAYER2_WEIGHTS_SZ (7800)   // (5 * 5 + 1) * 6 * 50    = 156 * 50 = 7800
#define LAYER3_WEIGHTS_SZ (125100) // (5 * 5 * 50 + 1) * 100  = ?
#define LAYER4_WEIGHTS_SZ (1010)   // (100 + 1) * 10          = ?

float Layer1_Weights_CPU[LAYER1_WEIGHTS_SZ] = {0.0};
float Layer2_Weights_CPU[LAYER2_WEIGHTS_SZ] = {0.0};
float Layer3_Weights_CPU[LAYER3_WEIGHTS_SZ] = {0.0};
float Layer4_Weights_CPU[LAYER4_WEIGHTS_SZ] = {0.0};

fixed16_t fixed_16_Layer1_Weights_CPU[LAYER1_WEIGHTS_SZ] = {0};
fixed16_t fixed_16_Layer2_Weights_CPU[LAYER2_WEIGHTS_SZ] = {0};
fixed16_t fixed_16_Layer3_Weights_CPU[LAYER3_WEIGHTS_SZ] = {0};
fixed16_t fixed_16_Layer4_Weights_CPU[LAYER4_WEIGHTS_SZ] = {0};

int main(int argc, char const *argv[])
{
  // initial layer 1 weight
  // Open the file for reading. b = This  is strictly  for  compatibility  with
  // C89 and has no effect
  FILE *pFile1 = fopen(FILE_NAME_WEIGHTS_1, "rb");
  // If error opening the file
  if (pFile1 != NULL) {
    fread(Layer1_Weights_CPU, sizeof(float), LAYER1_WEIGHTS_SZ, pFile1);
    fclose(pFile1);
  }else return 1;
  // initial layer 2 weight
  FILE *pFile2 = fopen(FILE_NAME_WEIGHTS_2, "rb");
  if (pFile2 != NULL) {
    fread(Layer2_Weights_CPU, sizeof(float), LAYER2_WEIGHTS_SZ, pFile2);
    fclose(pFile2);
  }else return 2;
  // initial layer 3 weight
  FILE *pFile3 = fopen(FILE_NAME_WEIGHTS_3, "rb");
  if (pFile3 != NULL) {
    fread(Layer3_Weights_CPU, sizeof(float), LAYER3_WEIGHTS_SZ, pFile3);
    fclose(pFile3);
  }else return 3;
  // initial layer 4 weight
  FILE *pFile4 = fopen(FILE_NAME_WEIGHTS_4, "rb");
  if (pFile4 != NULL) {
    fread(Layer4_Weights_CPU, sizeof(float), LAYER4_WEIGHTS_SZ, pFile4);
    fclose(pFile4);
  }else return 4;

  for (size_t i = 0; i < LAYER1_WEIGHTS_SZ; ++i)
  {
    fixed_16_Layer1_Weights_CPU[i] = float32_to_fixed16(Layer1_Weights_CPU[i]);
    printf("[FIXED]Layer1_Weights_CPU[%ld] = %f\r\n", i, fixed16_to_float32(fixed_16_Layer1_Weights_CPU[i]));
    printf("[FLOAT]Layer1_Weights_CPU[%ld] = %f\r\n", i, Layer1_Weights_CPU[i]);
  }
  for (size_t i = 0; i < LAYER2_WEIGHTS_SZ; ++i)
  {
    fixed_16_Layer2_Weights_CPU[i] = float32_to_fixed16(Layer2_Weights_CPU[i]);
  }
  for (size_t i = 0; i < LAYER3_WEIGHTS_SZ; ++i)
  {
    fixed_16_Layer3_Weights_CPU[i] = float32_to_fixed16(Layer3_Weights_CPU[i]);
  }
  for (size_t i = 0; i < LAYER4_WEIGHTS_SZ; ++i)
  {
    fixed_16_Layer4_Weights_CPU[i] = float32_to_fixed16(Layer4_Weights_CPU[i]);
  }

  FILE *pFile1_f16 = fopen(F16_FILE_NAME_WEIGHTS_1, "w+b");
  if (pFile1_f16 != NULL) {
    fwrite(fixed_16_Layer1_Weights_CPU, sizeof(fixed16_t), LAYER1_WEIGHTS_SZ, pFile1_f16);
    fclose(pFile1_f16);
  }else return 5;

  FILE *pFile2_f16 = fopen(F16_FILE_NAME_WEIGHTS_2, "w+b");
  if (pFile2_f16 != NULL) {
    fwrite(fixed_16_Layer2_Weights_CPU, sizeof(fixed16_t), LAYER2_WEIGHTS_SZ, pFile2_f16);
    fclose(pFile2_f16);
  }else return 6;

  FILE *pFile3_f16 = fopen(F16_FILE_NAME_WEIGHTS_3, "w+b");
  if (pFile3_f16 != NULL) {
    fwrite(fixed_16_Layer3_Weights_CPU, sizeof(fixed16_t), LAYER3_WEIGHTS_SZ, pFile3_f16);
    fclose(pFile3_f16);
  }else return 7;

  FILE *pFile4_f16 = fopen(F16_FILE_NAME_WEIGHTS_4, "w+b");
  if (pFile4_f16 != NULL) {
    fwrite(fixed_16_Layer4_Weights_CPU, sizeof(fixed16_t), LAYER4_WEIGHTS_SZ, pFile4_f16);
    fclose(pFile4_f16);
  }else return 8;


  FILE *Weights_C_FILE = fopen("Weights_created.c", "w+b");
  if (Weights_C_FILE != NULL) {
    fprintf(Weights_C_FILE, "#include \"Weights.h\"\r\n\r\n");
    fprintf(Weights_C_FILE, "weight_t Layer1_Weights_CPU[LAYER1_WEIGHTS_SZ] = {\r\n");
    for (size_t i = 0; i < LAYER1_WEIGHTS_SZ; i++)
    {
      fprintf(Weights_C_FILE, "{0x%04x}, ", fixed_16_Layer1_Weights_CPU[i]);
    }
    fprintf(Weights_C_FILE, "\r\n};\r\n");

    fprintf(Weights_C_FILE, "weight_t Layer2_Weights_CPU[LAYER2_WEIGHTS_SZ] = {\r\n");
    for (size_t i = 0; i < LAYER2_WEIGHTS_SZ; i++)
    {
      fprintf(Weights_C_FILE, "{0x%04x}, ", fixed_16_Layer2_Weights_CPU[i]);
    }
    fprintf(Weights_C_FILE, "\r\n};\r\n");

    fprintf(Weights_C_FILE, "weight_t Layer3_Weights_CPU[LAYER3_WEIGHTS_SZ] = {\r\n");
    for (size_t i = 0; i < LAYER3_WEIGHTS_SZ; i++)
    {
      fprintf(Weights_C_FILE, "{0x%04x}, ", fixed_16_Layer3_Weights_CPU[i]);
    }
    fprintf(Weights_C_FILE, "\r\n};\r\n");

    fprintf(Weights_C_FILE, "weight_t Layer4_Weights_CPU[LAYER4_WEIGHTS_SZ] = {\r\n");
    for (size_t i = 0; i < LAYER4_WEIGHTS_SZ; i++)
    {
      fprintf(Weights_C_FILE, "{0x%04x}, ", fixed_16_Layer4_Weights_CPU[i]);
    }
    fprintf(Weights_C_FILE, "\r\n};\r\n");

    fclose(pFile4_f16);
  }else return 9;

  printf("Finished!\r\n");


  return 0;
}
