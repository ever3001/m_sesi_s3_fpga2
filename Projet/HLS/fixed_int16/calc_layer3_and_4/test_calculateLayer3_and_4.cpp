#include <stdio.h>
#include <stdlib.h>

#include "test_calculateLayer3.hpp"

int main(int argc, char const *argv[])
{

  /* Layers Neurons*/
  int32_t Layer2_Neurons_CPU[LAYER2_NEURON_SZ] = {1};
  int32_t Layer4_Neurons_CPU[LAYER4_NEURON_SZ] = {0};

  printf("Before Calculate Layer3\n");

  calculateLayer3_and_4(Layer2_Neurons_CPU, Layer4_Neurons_CPU);

  for(int i = 0; i < LAYER4_NEURON_SZ; ++i){
      printf("%d\n", Layer4_Neurons_CPU[i]);
      // if(Layer4_Neurons_CPU[i] != Layer3_Neurons_CPU_Results[i]){
      //   printf("[VAL][%d]\r\n", i);
      // }
      // if(i % 12 == 0 && i < LAYER2_NEURON_SZ){
      // 	printf("Layer2_Weights_CPU[%d] = %d\n", i, Layer2_Weights_CPU[i]);
      // 	printf("Layer2_Neurons_CPU[%d] = %d\n", i, Layer2_Neurons_CPU[i]);
      // 	printf("Layer3_Neurons_CPU[%d] = %d\r\n", i, Layer3_Neurons_CPU[i]);
      // }
    }
  
  printf("After Calculate Layer3\n");

  return 0;
}
