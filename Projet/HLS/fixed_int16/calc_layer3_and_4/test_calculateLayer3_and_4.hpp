#ifndef _TEST_CALCULATE_LAYER_3_H_
#define _TEST_CALCULATE_LAYER_3_H_
#include <stdint.h>

#define LAYER2_WEIGHTS_SZ (7800)   // (5 * 5 + 1) * 6 * 50    = 156 * 50 = 7800
#define LAYER3_WEIGHTS_SZ (125100) // (5 * 5 * 50 + 1) * 100

#define LAYER2_NEURON_SZ (1014) // 6*13*13
#define LAYER3_NEURON_SZ (1250) // 50*5*5
#define LAYER4_NEURON_SZ (100)  // 10*10

int calculateLayer3_and_4(
  int32_t Layer2_Neurons_CPU[LAYER2_NEURON_SZ],
  int32_t Layer4_Neurons_CPU[LAYER4_NEURON_SZ]
);

#endif //! _TEST_CALCULATE_LAYER_3_H_
