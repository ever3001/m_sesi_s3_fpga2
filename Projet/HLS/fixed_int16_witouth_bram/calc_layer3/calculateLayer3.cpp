#include <stdint.h>
#define NBIT 8

#define TO_FIX(x)    ((int16_t)(x*(1<<NBIT)))
#define TO_FLOAT(x)  ((float)x / (float)(1<<NBIT))
#define FIX_MUL(x,y) ((x*y)/(1<<NBIT))
#define FIX_DIV(x,y) ((x/y)/(1<<NBIT))

#define LAYER2_WEIGHTS_SZ (7800)   // (5 * 5 + 1) * 6 * 50    = 156 * 50 = 7800

#define LAYER2_NEURON_SZ (1014) // 6*13*13
#define LAYER3_NEURON_SZ (1250) // 50*5*5

int16_t sigmoid_approx(int16_t x) {

  int16_t x3, x5;
  // 1.14393*x - 0.169472*x^3 + 0.0301283*x^5
  if (TO_FLOAT(x) > 1.5)
    return TO_FIX(1.7);
  if (TO_FLOAT(x) < -1.5)
    return TO_FIX(-1.7);
  x3 = x + x + x;
  x5 = x3 + x + x;
  return FIX_MUL(TO_FIX(1.14393), x) - FIX_MUL(TO_FIX(0.169472), x3) +
         FIX_MUL(TO_FIX(0.0301283), x5);

  // return TO_FIX(SIGMOID(TO_FLOAT(x)));
}

int calculateLayer3(
  int16_t Layer2_Neurons_CPU[LAYER2_NEURON_SZ],
  int16_t Layer2_Weights_CPU[LAYER2_WEIGHTS_SZ],
  int16_t Layer3_Neurons_CPU[LAYER3_NEURON_SZ]
){
#pragma HLS interface s_axilite port=return
#pragma HLS INTERFACE s_axilite PORT=Layer2_Weights_CPU bundle=CTRLS
#pragma HLS INTERFACE s_axilite PORT=Layer2_Neurons_CPU bundle=CTRLS
#pragma HLS INTERFACE s_axilite PORT=Layer3_Neurons_CPU bundle=CTRLS
  int32_t somme;
  int32_t mul;
  int i,j,k,m,n;
  for( i=0;i<50;++i){
//#pragma HLS LOOP_FLATTEN
    for(j=0;j<5;++j){
      for(k=0;k<5;++k){
        somme = Layer2_Weights_CPU[26*6*i];
//#pragma HLS PIPELINE
        for(m=0;m<5;++m){
//#pragma HLS UNROLL
          for(n=0;n<5;++n){
        	mul   = FIX_MUL(Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)	] ,Layer2_Neurons_CPU[13*13*0+13*(2*j+m)+(2*k+n)]);
            somme = somme + mul;
            mul   = FIX_MUL(Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+1], Layer2_Neurons_CPU[13*13*1+13*(2*j+m)+(2*k+n)]);
            somme = somme + mul;
            mul   = FIX_MUL(Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+2], Layer2_Neurons_CPU[13*13*2+13*(2*j+m)+(2*k+n)]);
            somme = somme + mul;
            mul   = FIX_MUL(Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+3], Layer2_Neurons_CPU[13*13*3+13*(2*j+m)+(2*k+n)]);
            somme = somme + mul;
            mul   = FIX_MUL(Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+4], Layer2_Neurons_CPU[13*13*4+13*(2*j+m)+(2*k+n)]);
            somme = somme + mul;
            mul   = FIX_MUL(Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+5], Layer2_Neurons_CPU[13*13*5+13*(2*j+m)+(2*k+n)]);
            somme = somme + mul;
          }
        }
        Layer3_Neurons_CPU[5*5*i+5*j+k] = sigmoid_approx(somme);
      }
    }
  }
  return Layer2_Weights_CPU[0];
}
