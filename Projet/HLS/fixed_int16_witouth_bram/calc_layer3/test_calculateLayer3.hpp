#ifndef _TEST_CALCULATE_LAYER_3_H_
#define _TEST_CALCULATE_LAYER_3_H_
#include <stdint.h>

#define LAYER2_WEIGHTS_SZ (7800)   // (5 * 5 + 1) * 6 * 50    = 156 * 50 = 7800

#define LAYER2_NEURON_SZ (1014) // 6*13*13
#define LAYER3_NEURON_SZ (1250) // 50*5*5

int calculateLayer3(
  int16_t Layer2_Neurons_CPU[LAYER2_NEURON_SZ],
  int16_t Layer2_Weights_CPU[LAYER2_WEIGHTS_SZ],
  int16_t Layer3_Neurons_CPU[LAYER3_NEURON_SZ]
);

#endif //! _TEST_CALCULATE_LAYER_3_H_
