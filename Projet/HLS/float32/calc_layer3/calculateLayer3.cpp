//#include <math.h>
#include <hls_math.h>

#define LAYER2_WEIGHTS_SZ (7800)   // (5 * 5 + 1) * 6 * 50    = 156 * 50 = 7800

#define LAYER2_NEURON_SZ (1014) // 6*13*13
#define LAYER3_NEURON_SZ (1250) // 50*5*5

#define SIGMOID(x) (1.7159*tanhf(0.66666667*x))

int calculateLayer3(
  float Layer2_Neurons_CPU[LAYER2_NEURON_SZ], 
  float Layer2_Weights_CPU[LAYER2_WEIGHTS_SZ], 
  float Layer3_Neurons_CPU[LAYER3_NEURON_SZ]
){
#pragma HLS INTERFACE s_axilite PORT=Layer3_Neurons_CPU bundle=CTRLS
#pragma HLS ARRAY_PARTITION variable=Layer3_Neurons_CPU cyclic factor=8 dim=1
#pragma HLS interface s_axilite port=return
#pragma HLS INTERFACE s_axilite PORT=Layer2_Weights_CPU bundle=CTRLS
#pragma HLS INTERFACE s_axilite PORT=Layer2_Neurons_CPU bundle=CTRLS
  float somme;
  float mul;
  int i,j,k,m,n;
  for( i=0;i<50;++i){
//#pragma HLS LOOP_FLATTEN
    for(j=0;j<5;++j){
      for(k=0;k<5;++k){
        somme = Layer2_Weights_CPU[26*6*i];
//#pragma HLS PIPELINE
        for(m=0;m<5;++m){
//#pragma HLS UNROLL
          for(n=0;n<5;++n){
        	mul   = Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)	] * Layer2_Neurons_CPU[13*13*0+13*(2*j+m)+(2*k+n)];
            somme = somme + mul;
            mul   = Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+1] * Layer2_Neurons_CPU[13*13*1+13*(2*j+m)+(2*k+n)];
            somme = somme + mul;
            mul   = Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+2] * Layer2_Neurons_CPU[13*13*2+13*(2*j+m)+(2*k+n)];
            somme = somme + mul;
            mul   = Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+3] * Layer2_Neurons_CPU[13*13*3+13*(2*j+m)+(2*k+n)];
            somme = somme + mul;
            mul   = Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+4] * Layer2_Neurons_CPU[13*13*4+13*(2*j+m)+(2*k+n)];
            somme = somme + mul;
            mul   = Layer2_Weights_CPU[26*6*i+1+6*(n+5*m)+5] * Layer2_Neurons_CPU[13*13*5+13*(2*j+m)+(2*k+n)];
            somme = somme + mul;
          }
        }
        Layer3_Neurons_CPU[5*5*i+5*j+k] = 1.7159*tanhf(0.66666667*somme);
      }
    }
  }
  return 0;
}
