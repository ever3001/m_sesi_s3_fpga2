#include <stdio.h>
#include <stdlib.h>

#include "test_calculateLayer3.h"

float Layer3_Neurons_CPU_Results[LAYER3_NEURON_SZ];


int main(int argc, char const *argv[])
{
  /* Layers Neurons*/
  float Layer2_Weights_CPU[LAYER2_WEIGHTS_SZ] = {1.0};
  float Layer2_Neurons_CPU[LAYER2_NEURON_SZ] = {1.0};
  float Layer3_Neurons_CPU[LAYER3_NEURON_SZ] = {0.0};

  printf("Before Calculate Layer3\n");

  calculateLayer3(Layer2_Neurons_CPU, Layer2_Weights_CPU, Layer3_Neurons_CPU);

  for(int i = 0; i < LAYER3_NEURON_SZ; ++i){
    printf("%f\n", Layer3_Neurons_CPU[i]);
    // TODO: Add test here
  }
  
  printf("After Calculate Layer3\n");

  return 0;
}
