// ==============================================================
// File generated on Wed Feb 17 16:52:13 +0100 2021
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
/***************************** Include Files *********************************/
#include "xcalculatelayer3_and_4.h"

/************************** Function Implementation *************************/
#ifndef __linux__
int XCalculatelayer3_and_4_CfgInitialize(XCalculatelayer3_and_4 *InstancePtr, XCalculatelayer3_and_4_Config *ConfigPtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(ConfigPtr != NULL);

    InstancePtr->Ctrl_bus_BaseAddress = ConfigPtr->Ctrl_bus_BaseAddress;
    InstancePtr->IsReady = XIL_COMPONENT_IS_READY;

    return XST_SUCCESS;
}
#endif

void XCalculatelayer3_and_4_Start(XCalculatelayer3_and_4 *InstancePtr) {
    u32 Data;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XCalculatelayer3_and_4_ReadReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_AP_CTRL) & 0x80;
    XCalculatelayer3_and_4_WriteReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_AP_CTRL, Data | 0x01);
}

u32 XCalculatelayer3_and_4_IsDone(XCalculatelayer3_and_4 *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XCalculatelayer3_and_4_ReadReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_AP_CTRL);
    return (Data >> 1) & 0x1;
}

u32 XCalculatelayer3_and_4_IsIdle(XCalculatelayer3_and_4 *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XCalculatelayer3_and_4_ReadReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_AP_CTRL);
    return (Data >> 2) & 0x1;
}

u32 XCalculatelayer3_and_4_IsReady(XCalculatelayer3_and_4 *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XCalculatelayer3_and_4_ReadReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_AP_CTRL);
    // check ap_start to see if the pcore is ready for next input
    return !(Data & 0x1);
}

void XCalculatelayer3_and_4_EnableAutoRestart(XCalculatelayer3_and_4 *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XCalculatelayer3_and_4_WriteReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_AP_CTRL, 0x80);
}

void XCalculatelayer3_and_4_DisableAutoRestart(XCalculatelayer3_and_4 *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XCalculatelayer3_and_4_WriteReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_AP_CTRL, 0);
}

u32 XCalculatelayer3_and_4_Get_return(XCalculatelayer3_and_4 *InstancePtr) {
    u32 Data;

    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Data = XCalculatelayer3_and_4_ReadReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_AP_RETURN);
    return Data;
}
void XCalculatelayer3_and_4_InterruptGlobalEnable(XCalculatelayer3_and_4 *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XCalculatelayer3_and_4_WriteReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_GIE, 1);
}

void XCalculatelayer3_and_4_InterruptGlobalDisable(XCalculatelayer3_and_4 *InstancePtr) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XCalculatelayer3_and_4_WriteReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_GIE, 0);
}

void XCalculatelayer3_and_4_InterruptEnable(XCalculatelayer3_and_4 *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XCalculatelayer3_and_4_ReadReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_IER);
    XCalculatelayer3_and_4_WriteReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_IER, Register | Mask);
}

void XCalculatelayer3_and_4_InterruptDisable(XCalculatelayer3_and_4 *InstancePtr, u32 Mask) {
    u32 Register;

    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    Register =  XCalculatelayer3_and_4_ReadReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_IER);
    XCalculatelayer3_and_4_WriteReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_IER, Register & (~Mask));
}

void XCalculatelayer3_and_4_InterruptClear(XCalculatelayer3_and_4 *InstancePtr, u32 Mask) {
    Xil_AssertVoid(InstancePtr != NULL);
    Xil_AssertVoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    XCalculatelayer3_and_4_WriteReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_ISR, Mask);
}

u32 XCalculatelayer3_and_4_InterruptGetEnabled(XCalculatelayer3_and_4 *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XCalculatelayer3_and_4_ReadReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_IER);
}

u32 XCalculatelayer3_and_4_InterruptGetStatus(XCalculatelayer3_and_4 *InstancePtr) {
    Xil_AssertNonvoid(InstancePtr != NULL);
    Xil_AssertNonvoid(InstancePtr->IsReady == XIL_COMPONENT_IS_READY);

    return XCalculatelayer3_and_4_ReadReg(InstancePtr->Ctrl_bus_BaseAddress, XCALCULATELAYER3_AND_4_CTRL_BUS_ADDR_ISR);
}

