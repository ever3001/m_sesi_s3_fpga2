// ==============================================================
// File generated on Wed Feb 17 16:52:13 +0100 2021
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef XCALCULATELAYER3_AND_4_H
#define XCALCULATELAYER3_AND_4_H

#ifdef __cplusplus
extern "C" {
#endif

/***************************** Include Files *********************************/
#ifndef __linux__
#include "xil_types.h"
#include "xil_assert.h"
#include "xstatus.h"
#include "xil_io.h"
#else
#include <stdint.h>
#include <assert.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <unistd.h>
#include <stddef.h>
#endif
#include "xcalculatelayer3_and_4_hw.h"

/**************************** Type Definitions ******************************/
#ifdef __linux__
typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
#else
typedef struct {
    u16 DeviceId;
    u32 Ctrl_bus_BaseAddress;
} XCalculatelayer3_and_4_Config;
#endif

typedef struct {
    u32 Ctrl_bus_BaseAddress;
    u32 IsReady;
} XCalculatelayer3_and_4;

/***************** Macros (Inline Functions) Definitions *********************/
#ifndef __linux__
#define XCalculatelayer3_and_4_WriteReg(BaseAddress, RegOffset, Data) \
    Xil_Out32((BaseAddress) + (RegOffset), (u32)(Data))
#define XCalculatelayer3_and_4_ReadReg(BaseAddress, RegOffset) \
    Xil_In32((BaseAddress) + (RegOffset))
#else
#define XCalculatelayer3_and_4_WriteReg(BaseAddress, RegOffset, Data) \
    *(volatile u32*)((BaseAddress) + (RegOffset)) = (u32)(Data)
#define XCalculatelayer3_and_4_ReadReg(BaseAddress, RegOffset) \
    *(volatile u32*)((BaseAddress) + (RegOffset))

#define Xil_AssertVoid(expr)    assert(expr)
#define Xil_AssertNonvoid(expr) assert(expr)

#define XST_SUCCESS             0
#define XST_DEVICE_NOT_FOUND    2
#define XST_OPEN_DEVICE_FAILED  3
#define XIL_COMPONENT_IS_READY  1
#endif

/************************** Function Prototypes *****************************/
#ifndef __linux__
int XCalculatelayer3_and_4_Initialize(XCalculatelayer3_and_4 *InstancePtr, u16 DeviceId);
XCalculatelayer3_and_4_Config* XCalculatelayer3_and_4_LookupConfig(u16 DeviceId);
int XCalculatelayer3_and_4_CfgInitialize(XCalculatelayer3_and_4 *InstancePtr, XCalculatelayer3_and_4_Config *ConfigPtr);
#else
int XCalculatelayer3_and_4_Initialize(XCalculatelayer3_and_4 *InstancePtr, const char* InstanceName);
int XCalculatelayer3_and_4_Release(XCalculatelayer3_and_4 *InstancePtr);
#endif

void XCalculatelayer3_and_4_Start(XCalculatelayer3_and_4 *InstancePtr);
u32 XCalculatelayer3_and_4_IsDone(XCalculatelayer3_and_4 *InstancePtr);
u32 XCalculatelayer3_and_4_IsIdle(XCalculatelayer3_and_4 *InstancePtr);
u32 XCalculatelayer3_and_4_IsReady(XCalculatelayer3_and_4 *InstancePtr);
void XCalculatelayer3_and_4_EnableAutoRestart(XCalculatelayer3_and_4 *InstancePtr);
void XCalculatelayer3_and_4_DisableAutoRestart(XCalculatelayer3_and_4 *InstancePtr);
u32 XCalculatelayer3_and_4_Get_return(XCalculatelayer3_and_4 *InstancePtr);


void XCalculatelayer3_and_4_InterruptGlobalEnable(XCalculatelayer3_and_4 *InstancePtr);
void XCalculatelayer3_and_4_InterruptGlobalDisable(XCalculatelayer3_and_4 *InstancePtr);
void XCalculatelayer3_and_4_InterruptEnable(XCalculatelayer3_and_4 *InstancePtr, u32 Mask);
void XCalculatelayer3_and_4_InterruptDisable(XCalculatelayer3_and_4 *InstancePtr, u32 Mask);
void XCalculatelayer3_and_4_InterruptClear(XCalculatelayer3_and_4 *InstancePtr, u32 Mask);
u32 XCalculatelayer3_and_4_InterruptGetEnabled(XCalculatelayer3_and_4 *InstancePtr);
u32 XCalculatelayer3_and_4_InterruptGetStatus(XCalculatelayer3_and_4 *InstancePtr);

#ifdef __cplusplus
}
#endif

#endif
