// ==============================================================
// File generated on Wed Feb 17 16:52:13 +0100 2021
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2018.3 (64-bit)
// SW Build 2405991 on Thu Dec  6 23:38:27 MST 2018
// IP Build 2404404 on Fri Dec  7 01:43:56 MST 2018
// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __linux__

#include "xstatus.h"
#include "xparameters.h"
#include "xcalculatelayer3_and_4.h"

extern XCalculatelayer3_and_4_Config XCalculatelayer3_and_4_ConfigTable[];

XCalculatelayer3_and_4_Config *XCalculatelayer3_and_4_LookupConfig(u16 DeviceId) {
	XCalculatelayer3_and_4_Config *ConfigPtr = NULL;

	int Index;

	for (Index = 0; Index < XPAR_XCALCULATELAYER3_AND_4_NUM_INSTANCES; Index++) {
		if (XCalculatelayer3_and_4_ConfigTable[Index].DeviceId == DeviceId) {
			ConfigPtr = &XCalculatelayer3_and_4_ConfigTable[Index];
			break;
		}
	}

	return ConfigPtr;
}

int XCalculatelayer3_and_4_Initialize(XCalculatelayer3_and_4 *InstancePtr, u16 DeviceId) {
	XCalculatelayer3_and_4_Config *ConfigPtr;

	Xil_AssertNonvoid(InstancePtr != NULL);

	ConfigPtr = XCalculatelayer3_and_4_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		InstancePtr->IsReady = 0;
		return (XST_DEVICE_NOT_FOUND);
	}

	return XCalculatelayer3_and_4_CfgInitialize(InstancePtr, ConfigPtr);
}

#endif

