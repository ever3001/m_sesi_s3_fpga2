----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Ever ATILANO
-- 
-- Create Date: 01/14/2021 11:21:03 AM
-- Design Name: 
-- Module Name: CALC_ADDR - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: Calculate 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE ieee.numeric_std.ALL;

ENTITY CALC_ADDR IS
    PORT (
        i : IN STD_LOGIC_VECTOR (12 DOWNTO 0);
        j : IN STD_LOGIC_VECTOR (12 DOWNTO 0);
        k : IN STD_LOGIC_VECTOR (12 DOWNTO 0);
        m : IN STD_LOGIC_VECTOR (12 DOWNTO 0);
        n : IN STD_LOGIC_VECTOR (12 DOWNTO 0);
        o : IN STD_LOGIC_VECTOR (12 DOWNTO 0);
        p : IN STD_LOGIC_VECTOR (12 DOWNTO 0);
        -- 10 bits
        ADDRESS_LAYER2_NEURON : OUT STD_LOGIC_VECTOR (12 DOWNTO 0);
        -- 11 bits
        ADDRESS_LAYER3_NEURON : OUT STD_LOGIC_VECTOR (12 DOWNTO 0);
        -- 13 bits
        ADDRESS_LAYER2_WEIGHT : OUT STD_LOGIC_VECTOR (12 DOWNTO 0)
    );
END CALC_ADDR;

ARCHITECTURE Behavioral OF CALC_ADDR IS

BEGIN
    -- Layer2_Weights_CPU[26*6*i+6*(n+5*m)+o+p]
    ADDRESS_LAYER2_WEIGHT <= std_logic_vector(
        ( unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) ) +
        ( (unsigned(n) + unsigned(m) +unsigned(m) + unsigned(m) + unsigned(m) + unsigned(m)) + (unsigned(n) + unsigned(m) +unsigned(m) + unsigned(m) + unsigned(m) + unsigned(m)) + (unsigned(n) + unsigned(m) +unsigned(m) + unsigned(m) + unsigned(m) + unsigned(m)) + (unsigned(n) + unsigned(m) +unsigned(m) + unsigned(m) + unsigned(m) + unsigned(m)) + (unsigned(n) + unsigned(m) +unsigned(m) + unsigned(m) + unsigned(m) + unsigned(m)) + (unsigned(n) + unsigned(m) +unsigned(m) + unsigned(m) + unsigned(m) + unsigned(m)) ) +
        ( unsigned(o) ) + 
        ( unsigned(p) )
    );
    -- Layer2_Neurons_CPU[13*13*o+13*(2*j+m)+(2*k+n)]
    ADDRESS_LAYER2_NEURON <= std_logic_vector(
     ( unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) + unsigned(o) ) +
     ( (unsigned(j) + unsigned(j) + unsigned(m)) + (unsigned(j) + unsigned(j) + unsigned(m)) + (unsigned(j) + unsigned(j) + unsigned(m)) + (unsigned(j) + unsigned(j) + unsigned(m)) + (unsigned(j) + unsigned(j) + unsigned(m)) + (unsigned(j) + unsigned(j) + unsigned(m)) + (unsigned(j) + unsigned(j) + unsigned(m)) + (unsigned(j) + unsigned(j) + unsigned(m)) + (unsigned(j) + unsigned(j) + unsigned(m)) + (unsigned(j) + unsigned(j) + unsigned(m)) + (unsigned(j) + unsigned(j) + unsigned(m)) + (unsigned(j) + unsigned(j) + unsigned(m)) + (unsigned(j) + unsigned(j) + unsigned(m)) )+
     ( unsigned(k) + unsigned(k) + unsigned(n) )
    );
    -- Layer3_Neurons_CPU[5*5*i+5*j+k]
    ADDRESS_LAYER3_NEURON <= STD_LOGIC_VECTOR(
        (unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i) + unsigned(i)) +
        (unsigned(j) + unsigned(j) + unsigned(j) + unsigned(j) + unsigned(j)) +
        (unsigned(k))
        );

END Behavioral;