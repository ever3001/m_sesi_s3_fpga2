----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 01/14/2021 01:34:00 PM
-- Design Name: 
-- Module Name: TB_CALC_ADDR - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;

ENTITY TB_CALC_ADDR IS END;

ARCHITECTURE Behavioral OF TB_CALC_ADDR IS
  --! ########   Signals for component    ########
  SIGNAL i : STD_LOGIC_VECTOR (12 DOWNTO 0):= (others => '0');
  SIGNAL j : STD_LOGIC_VECTOR (12 DOWNTO 0):= (others => '0');
  SIGNAL k : STD_LOGIC_VECTOR (12 DOWNTO 0):= (others => '0');
  SIGNAL m : STD_LOGIC_VECTOR (12 DOWNTO 0):= (others => '0');
  SIGNAL n : STD_LOGIC_VECTOR (12 DOWNTO 0):= (others => '0');
  SIGNAL o : STD_LOGIC_VECTOR (12 DOWNTO 0):= (others => '0');
  SIGNAL p : STD_LOGIC_VECTOR (12 DOWNTO 0):= (others => '0');
  SIGNAL ADDRESS_LAYER2_NEURON : STD_LOGIC_VECTOR (12 DOWNTO 0):= (others => '0');
  SIGNAL ADDRESS_LAYER3_NEURON : STD_LOGIC_VECTOR (12 DOWNTO 0):= (others => '0');
  SIGNAL ADDRESS_LAYER2_WEIGHT : STD_LOGIC_VECTOR (12 DOWNTO 0):= (others => '0');

  SIGNAL STOP_SIM : STD_LOGIC := '0'; -- Flag to stop the simulation
  SIGNAL TEST : STD_LOGIC := '0'; -- Indicates if a test is running
  SIGNAL ERROR_SIGNAL : STD_LOGIC := '0'; -- Indicates if an assert is incorrect

BEGIN
  CALC_ADDR_0 : ENTITY work.CALC_ADDR
    PORT MAP(
      i => i,
      j => j,
      k => k,
      m => m,
      n => n,
      o => o,
      p => p,
      ADDRESS_LAYER2_NEURON => ADDRESS_LAYER2_NEURON,
      ADDRESS_LAYER3_NEURON => ADDRESS_LAYER3_NEURON,
      ADDRESS_LAYER2_WEIGHT => ADDRESS_LAYER2_WEIGHT
    );

  PROCESS
  BEGIN
    i <= "0000000000000";
    j <= "0000000000000";
    k <= "0000000000000";
    m <= "0000000000000";
    n <= "0000000000000";
    o <= "0000000000000";
    p <= "0000000000000";

    WAIT FOR 20 ns;

    -- REPORT "[TEST_01] INIT : Test if the divisor works";
    -- ############ Signals initialization
    TEST <= '1';
    ERROR_SIGNAL <= '0';

    i <= "0000000000000";
    j <= "0000000000000";
    k <= "0000000000000";
    m <= "0000000000000";
    n <= "0000000000000";
    o <= "0000000000000";
    p <= "0000000000001";

    WAIT FOR 20 ns;

    i <= "0000000000000";
    j <= "0000000000000";
    k <= "0000000000000";
    m <= "0000000000000";
    n <= "0000000000000";
    o <= "0000000000001";
    p <= "0000000000001";

    WAIT FOR 20 ns;

    i <= "0000000000000";
    j <= "0000000000000";
    k <= "0000000000000";
    m <= "0000000000000";
    n <= "0000000000001";
    o <= "0000000000001";
    p <= "0000000000001";

    WAIT FOR 20 ns;

    i <= "0000000000000";
    j <= "0000000000000";
    k <= "0000000000000";
    m <= "0000000000001";
    n <= "0000000000001";
    o <= "0000000000001";
    p <= "0000000000001";

    WAIT FOR 20 ns;

    i <= "0000000000000";
    j <= "0000000000000";
    k <= "0000000000001";
    m <= "0000000000001";
    n <= "0000000000001";
    o <= "0000000000001";
    p <= "0000000000001";

    WAIT FOR 20 ns;

    i <= "0000000000000";
    j <= "0000000000001";
    k <= "0000000000001";
    m <= "0000000000001";
    n <= "0000000000001";
    o <= "0000000000001";
    p <= "0000000000001";

    WAIT FOR 20 ns;

    i <= "0000000000001";
    j <= "0000000000000";
    k <= "0000000000000";
    m <= "0000000000000";
    n <= "0000000000000";
    o <= "0000000000000";
    p <= "0000000000000";

    WAIT FOR 20 ns;

    i <= "0000000000001";
    j <= "0000000000000";
    k <= "0000000000000";
    m <= "0000000000000";
    n <= "0000000000000";
    o <= "0000000000000";
    p <= "0000000000001";

    WAIT FOR 20 ns;

    i <= "0000000000001";
    j <= "0000000000000";
    k <= "0000000000000";
    m <= "0000000000000";
    n <= "0000000000000";
    o <= "0000000000001";
    p <= "0000000000001";

    WAIT FOR 20 ns;
    TEST <= '0';
    -- REPORT "[TEST_01] FINISHED";

    -- DON'T DELETE THE NEXT TWO LINES. THESE LINES STOPS THE SIMULATION
    STOP_SIM <= '1';
    WAIT;
  END PROCESS;
END ARCHITECTURE Behavioral;