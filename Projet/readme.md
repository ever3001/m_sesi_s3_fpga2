# Projet FPGA2 : Réseau de neurones de reconnaissance de caractères sur ZedBoard

## Transformation de BMP à binaire

[Image en Binaire 0 1](https://www.dcode.fr/image-binaire)

## Compiling the project

Before compiling the project, the code must be setup.

```bash
make all
```

## Compilation for MCU

### Add math library for SDK

The math "m" option needs to be specified in the Libraries in the C/C++ Build Settings.

1. Right-click in the project > Properties  
2. C/C++ Build > Settings > Tool Settings > ARM v7 gcc linker > Libraries
3. Add...
4. Fill with "m"
5. OK
6. OK

![SDK image](./math_options_201211160312449257.jpg "sdk image")

## gprof results

```bash
# i = 10000
%   cumulative   self              self     total
time   seconds   seconds    calls  us/call  us/call  name
53.79      9.63     9.63    10000   962.77   962.77  calculateLayer3
39.86     16.76     7.14    10000   713.57   713.57  calculateLayer4
6.26     17.88     1.12    10000   112.09   112.09  calculateLayer2
0.06     17.89     0.01    10000     1.00     1.00  InitHostMem
0.06     17.90     0.01    10000     1.00     1.00  calculateLayer5
0.06     17.91     0.01                             main
0.00     17.91     0.00    10000     0.00     0.00  calculateLayer1

# i = 100000
Each sample counts as 0.01 seconds.
%   cumulative   self              self     total
time   seconds   seconds    calls  us/call  us/call  name
53.01     89.24    89.24   100000   892.41   892.41  calculateLayer3
39.56    155.84    66.60   100000   666.03   666.03  calculateLayer4
7.21    167.98    12.14   100000   121.40   121.40  calculateLayer2
0.23    168.36     0.38   100000     3.80     3.80  calculateLayer5
0.04    168.42     0.06   100000     0.60     0.60  InitHostMem
0.03    168.47     0.05                             main
0.00    168.47     0.00   100000     0.00     0.00  calculateLayer1
```