# FPGA 2

Équipe:

- Ever ATILANO
- Lucie Tan
- Jörg Ehmer

## ZedBoard Zynq-7000

- [ZedBoard Zynq-7000 ARM/FPGA SoC Development Board](https://www.xilinx.com/products/boards-and-kits/1-elhabt.html.html)  
- [Reference manual](https://reference.digilentinc.com/_media/zedboard:zedboard_ug.pdf)  
- [Schematics](https://reference.digilentinc.com/_media/zedboard:zedboard_sch.pdf)  

## Cours
- [Cours 1 : Introduction - Systèmes Embarqués - Conception par profilage](./Cours/Cours_1_Introduction_Systemes_Embarques_Conception_par_profilage.pdf)  
- [Cours 2 : Codesign - Décomposition - Parallélisme](./Cours/Cours_2_Codesign_Decomposition_Parallelisme.pdf)  
- [Cours 3 : Partitionnement, heuristiques, Pareto](./Cours/Cours_3_Partitionnement_heuristiques_Pareto.pdf)  
- [Cours 4 : HLS](./Cours/Cours_4_HLS.pdf)  
- [Cours 5 : SDSoC](./Cours/Cours_5_SDSoC.pdf)  
- [Introduction aux outils SDx](./Cours/Introduction_aux_outils_SDx.pdf)

## Projet