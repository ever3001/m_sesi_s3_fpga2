# TME_03 Dessigning with Vivado HLS

## Quick setup

1. Open Vitis HLS
2. Add files matrix_mult.cpp et matrix_mult.h

Real constraint file

```
set_property PACKAGE_PIN T22        [get_ports {LEDs_out_0[0]}]
set_property IOSTANDARD LVCMOS33    [get_ports {LEDs_out_0[0]}]
set_property PACKAGE_PIN T21        [get_ports {LEDs_out_0[1]}]
set_property IOSTANDARD LVCMOS33    [get_ports {LEDs_out_0[1]}]
set_property PACKAGE_PIN U22        [get_ports {LEDs_out_0[2]}]
set_property IOSTANDARD LVCMOS33    [get_ports {LEDs_out_0[2]}]
set_property PACKAGE_PIN U21        [get_ports {LEDs_out_0[3]}]
set_property IOSTANDARD LVCMOS33    [get_ports {LEDs_out_0[3]}]
set_property PACKAGE_PIN V22        [get_ports {LEDs_out_0[4]}]
set_property IOSTANDARD LVCMOS33    [get_ports {LEDs_out_0[4]}]
set_property PACKAGE_PIN W22        [get_ports {LEDs_out_0[5]}]
set_property IOSTANDARD LVCMOS33    [get_ports {LEDs_out_0[5]}]
set_property PACKAGE_PIN U19        [get_ports {LEDs_out_0[6]}]
set_property IOSTANDARD LVCMOS33    [get_ports {LEDs_out_0[6]}]
set_property PACKAGE_PIN U14        [get_ports {LEDs_out_0[7]}]
set_property IOSTANDARD LVCMOS33    [get_ports {LEDs_out_0[7]}]
```

Added to file "led_controller.h" the follow line

```c
#include "xil_types.h"
#include "xstatus.h"
/* Added the follow header */
#include "xil_io.h"
```
